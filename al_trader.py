'''Module al_trader.py'''
'''Name: Rishav Guha'''
'''FSUID: rg14d'''

from pandas import read_csv,DataFrame,concat,Series
from keras.models import Sequential, load_model
from keras.layers import Dense, SimpleRNN
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
from sklearn.externals import joblib

from math import sqrt

from matplotlib import pyplot
import numpy as np

name_model_save = "al_model.h5"
name_scaler_save = "al_scaler.save"

show_my_plot = False
to_save = False
name_dataset = "aap_dataset.csv"
name_testset = "aap_test.csv"
n_skip = 0
n_lag = 7
n_seq = 7
n_test = 0.2
n_epochs = 20
n_batch = 1
n_neurons = 100

def tsToSupervised(data, n_in=1, n_out=1, dropnan=True):

    if type(data) == list:
        numVars = 1
    else:
        numVars = data.shape[1]

    df = DataFrame(data)
    cols, names = [], []

    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('feature%d(t-%d)' % (j+1, i)) for j in range(numVars)]

    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('feature%d(t)' % (j+1)) for j in range(numVars)]
        else:
            names += [('feature%d(t+%d)' % (j+1, i)) for j in range(numVars)]

    conc = concat(cols, axis=1)
    conc.columns = names

    if dropnan:
        conc.dropna(inplace=True)
    return conc

def inverse_difference(last_ob, pred):
    inverted = list()
    inverted.append(pred[0] + last_ob)

    for i in range(1, len(pred)):
        inverted.append(pred[i] + inverted[i-1])
    return inverted

model = 0
scaler = 0

#greeting
print("\n\nWelcome to AlTrader. This is is a program that allows you to tain a model to train and predict stock and cryptocurrency"
      "prices and then get the optimal time to sell and buy stocks to get the most profit in the selected time period\n")

#user inputs
enter_choice = input("Do you want to train a new model or load the existing model and scaler?(train/load):[train]")

if str(enter_choice) == "" or str(enter_choice) == "train":

    enter_dataset = input('Please enter name of dataset:[aap_dataset.csv] ')

    if str(enter_dataset) != "":
        name_dataset = str(enter_dataset)

    enter_skip = input('Please enter number of records to skip:[0]')

    if str(enter_skip) != "":
        n_skip = int(enter_skip)

    enter_lag = input('Please enter number of past time steps to consider:[7]')

    if str(enter_lag) != "":
        n_lag = int(enter_lag)

    enter_seq = input('Please enter number of future time steps to predict:[7]')

    if str(enter_seq) != "":
        n_seq = int(enter_seq)

    enter_test = input('Please enter percentage of data to be used for validation(0.1-0.9):[0.2]')

    if str(enter_test) != "":
        n_test = float(enter_test)

    enter_epochs = input('Please enter number of epochs to train for:[20]')

    if str(enter_epochs) != "":
        n_epochs = int(enter_epochs)

    enter_batch = input('Please enter batch size:[1]')

    if str(enter_batch) != "":
        n_batch = int(enter_batch)

    enter_neurons = input('Please enter number of neurons:[100]')

    if str(enter_neurons) != "":
        n_neurons = int(enter_neurons)

    enter_save = input("Do you want to save the model and scaler after you're done?(yes/no):[no]")

    if str(enter_save) != "" and str(enter_save) == "yes":
        to_save = True

    enter_plot = input("Do you want to view the plot of the real and predicted values after training?:[no]")

    if str(enter_plot) != "" and str(enter_plot) == "yes":
        show_my_plot = True


    dataset = read_csv(name_dataset, index_col=0)

    dataset.index.name = "Date"

    dataset = dataset[n_skip:]

    dataset.fillna(0, inplace=True)

    dataset.to_csv("temp.csv")

    dataset = read_csv("temp.csv", header=0, index_col=0)

    values = dataset.values

    values = values.astype('float32')

    #convert to differenced series
    diff = []

    for i in range(1, len(values)):
        value = values[i] - values[i - 1]
        diff.append(value)

    diff = Series(diff)

    values = diff.values

    values = values.reshape(len(values), 1)

    #normalize values
    scaler = MinMaxScaler(feature_range=(-1,1))
    values = scaler.fit_transform(values)
    values = values.reshape(len(values), 1)

    #convert from time series to supervised problem
    reframed = tsToSupervised(values, n_lag, n_seq)
    values = reframed.values

    reframed.to_csv("test.csv")

    #split into train and test
    n_test = int(n_test*len(values))

    train = values[0:-n_test]
    test = values[-n_test:]

    #setup model
    X_train = train[:, 0:n_lag]
    Y_train = train[:, n_lag:]

    X_test1 = test[:, 0:n_lag]
    Y_test1 = test[:, n_lag:]

    X_train = X_train.reshape(X_train.shape[0], 1, X_train.shape[1])
    X_test1 = X_test1.reshape(X_test1.shape[0], 1, X_test1.shape[1])

    model = Sequential()
    model.add(SimpleRNN(n_neurons, batch_input_shape=(n_batch, X_train.shape[1], X_train.shape[2]), stateful=True))
    model.add(Dense(Y_train.shape[1]))
    model.compile(loss='mean_squared_error', optimizer='adam')

    #fit model
    '''
    for i in range(n_epochs):
            model.fit(X_train, Y_train, epochs=1, batch_size=n_batch, validation_data=(X_test1, Y_test1),verbose=2, shuffle=False)
            model.reset_states()
    '''
    history = model.fit(X_train, Y_train, epochs=n_epochs, batch_size=n_batch, validation_data=(X_test1, Y_test1), verbose=2, shuffle=True)

    #save if desired
    if to_save:
        model.save(name_model_save)
        joblib.dump(scaler, name_scaler_save)

    #for debugging
    #pyplot.plot(history.history['loss'], label='train')
    #pyplot.plot(history.history['val_loss'], label='test')
    #pyplot.legend()
    #pyplot.show()

    #get predictions
    predictions = []
    for i in range(len(test)):
            X_test = test[i, 0:n_lag]
            Y_test = test[i, n_lag:]

            # make single prediction
            X_test = X_test.reshape(1, 1, len(X_test))

            pred = model.predict(X_test, batch_size=n_batch)

            # store
            predictions.append([x for x in pred[0, :]])

    inverse = []

    #print (len(predictions), len(dataset), (n_test))

    for i in range(len(predictions)):
            pred = np.array(predictions[i])
            pred = pred.reshape(1, len(pred))
            # invert scaling and diff in reverse order
            inverse_scaled = scaler.inverse_transform(pred)
            inverse_scaled = inverse_scaled[0, :]

            index = len(dataset) - n_test-(n_seq-1) + i - 1
            last_ob = dataset.values[index]
            inv_diff = inverse_difference(last_ob, inverse_scaled)

            inverse.append(inv_diff)

    #set predictions as the undiffed and unscaled values
    predictions = inverse

    real = [x[n_lag:] for x in test]

    inverse = []

    for i in range(len(real)):
            real1 = np.array(real[i])
            real1 = real1.reshape(1, len(real1))
            # invert scaling and diff in reverse order
            inverse_scaled = scaler.inverse_transform(real1)
            inverse_scaled = inverse_scaled[0, :]

            index = len(dataset) - n_test-(n_seq-1) + i - 1
            last_ob = dataset.values[index]
            inv_diff = inverse_difference(last_ob, inverse_scaled)

            inverse.append(inv_diff)

    real = inverse

    for i in range(n_seq):
            realVal = [x[i] for x in real]
            predVal = [x[i] for x in predictions]
            rmse = sqrt(mean_squared_error(realVal, predVal))
            print('t+%d RMSE: %f' % ((i+1), rmse))

    #plot for debugging
    pyplot.plot(dataset.values)
    for i in range(len(predictions)):
        off_s = len(dataset) - n_test -(n_seq-1) + i - 1
        off_e = off_s + len(predictions[i]) + 1
        xaxis = [x for x in range(off_s, off_e)]
        yaxis = [dataset.values[off_s]] + predictions[i]
        pyplot.plot(xaxis, yaxis, color='red')
    # show the plot
    #pyplot.show()

else:
    model = load_model(name_model_save)
    scaler = joblib.load(name_scaler_save)

enter_test_name = input("Enter the name of the dataset with which you want the model to predict from:[aap_test.csv]")

if str(enter_test_name) != "":
    name_testset = str(enter_test_name)

test_dataset = read_csv(name_testset, header=0, index_col=0)

values = test_dataset.values

values = values.astype('float32')


#convert to differenced series
diff = []

for i in range(1, len(values)):
    value = values[i] - values[i - 1]
    diff.append(value)

diff = Series(diff)

values = diff.values

values = values.reshape(len(values), 1)

values = scaler.transform(values)

values = values.reshape(len(values), 1)

#convert from time series to supervised problem
reframed = tsToSupervised(values, n_lag, n_seq)
values = reframed.values


test1 = np.array([values[0]])

X_test1 = test1[:, 0:n_lag]
Y_test1 = test1[:, n_lag:]

X_test1 = X_test1.reshape(X_test1.shape[0], 1, X_test1.shape[1])

predicted = model.predict(X_test1, batch_size=1, verbose=2)

#print(predicted)

inverse_scaled = scaler.inverse_transform(predicted)
inverse_scaled = inverse_scaled[0, :]

#index = len(dataset)-(n_seq-1) - 1
last_ob = test_dataset.values[0]
inv_diff = inverse_difference(last_ob, inverse_scaled)

predicted = inv_diff
#print(predicted)

time_set = []

for i in predicted:
    time_set.append(i[0])

print('\n\nThe predicted values for the next ',n_lag,' days are: \n', time_set)

if(time_set.index(max(time_set)) < time_set.index(min(time_set))):
    print("\nAccording to the model, you should sell ", time_set.index(max(time_set))+1
          ," and buy ", time_set.index(min(time_set))+1," days from now to accrue max profit"
          ," in the given time period")
else:
    print("\nAccording to the model, you should buy ", time_set.index(min(time_set)) + 1
          , " and sell ", time_set.index(max(time_set)) + 1, " days from now to accrue max profit"
          , " in the given time period")

if(show_my_plot):
    pyplot.show()