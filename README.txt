Name: Rishav Guha

FSUID: rg14d

Problem: 
    To find the optimal time to sell and buy a stock or a cryptocurrency in a given time period so as to get the most profit or least loss

UI: 
    Basic commandline interface. Type in the inputs to all the prompts. If nothing is typed and enter is hit, the default value, displayed within brackets, is displayed. Some prompts have acceptable values/ranges in parentheses.

Libraries Used:
    pandas
    keras(over tensorflow)
    sklearn
    matplotlib
    numpy

Other Resources Used:
    Bitcoin daily price dataset from Kaggle
    -https://www.kaggle.com/sudalairajkumar/cryptocurrencypricehistory#bitcoin_dataset.csv

    Apple daily price dataset from Kaggle
    -https://www.kaggle.com/camnugent/sandp500/version/4

Extra features implemented:
    Ability to save and load trained models to use with a test set
    Ability to set custom time periods for models

Separation of Work
    Rishav